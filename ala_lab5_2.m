minNodes = 10;
maxNodes = 15;

minEdges = 25;
maxEdges = 50;

nodesAm=randi(maxNodes - minNodes + 1) + minNodes - 1;
edgesAm = randi(maxEdges - minEdges + 1)+ minEdges - 1;

cflag = 1;
while cflag
    A = randi(nodesAm,edgesAm, 2);

    for i=1:edgesAm
        if A(i,1)==A(i,2)
            tmp = [1:A(i)-1,A(i)+1:nodesAm];
            A(i,2)=tmp(randi(nodesAm-1));
        end
    end

    for i=1:nodesAm
        if sum(A(:,1)==i)==0
            cflag = 1;
            %disp('oops')
            break;
        else
            cflag = 0;
        end
    end
end

G = digraph(A(:,1),A(:,2));

plot(G,'Layout','force','MarkerSize',7,'NodeFontSize',10, ...
    EdgeColor='black');

M = zeros(nodesAm);

for i=1:nodesAm
    for j=1:nodesAm
        M(i,j) = sum(all(A==[j,i],2))/sum(A(:,1)==j);
    end
end

%альтернативный способ нахождения V
%{
eps = 0.0001;
M_old = zeros(nodesAm);
while max(max(M-M_old))>eps
    M_old = M;
    M=M*M;
end

V = M(:,1);
%}

[V,D]=eig(M);
V=abs(V(:,1));

[out,idx] = sort(V,'descend');

disp('Ranked nodes:')
disp(idx)
%disp(out)
%{
Ranked nodes:
     4
     8
     7
     2
    11
    12
     1
     9
     5
     6
     3
    10
    13

%}

